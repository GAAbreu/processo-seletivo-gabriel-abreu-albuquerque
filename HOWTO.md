Por ser minha primeira vez trabalhando com as tecnologias propostas tive muita dificuldade de fazer o proposto. Não há implementação com banco de dados nesse projeto, logo os dados são armazenados somente em cache, e utilizei o spectre como framework alternativo ao bootstrap.

- O app foi essencialmente construído em Vue, havendo dois principais arquivos responsáveis pela operação: App.vue(módulo principal) e fun.vue(componente do app). Como não tive tempo suficiente para estudar a solução de backend sugerida para o armazenamento dos dados, utilizei um array, que guarda cada funcionário e seus atributos como um objeto.

Para executar a aplicação:

- Para executar o sistema em windows, deve ser utilizado o comando "npm run serve". Dessa maneira o app poderá rodar tanto em localhost como na rede.

- Ao rodar o app existem dois botões logo na tela inicial: "cadastrar novos funcionários" e "ver funcionários". Ambos são botões do tipo show/hide que abrem os módulos que mostram os funcionários.

- No cadastro de funcionários há 5 campos para a inserção dos dados. Após a iserção dos dados deve-se clicar no botão "Adicionar funcionário" para armazená-lo na memória. Para exibir os funcionários cadastrados deve-se clicar em ver funcionários.(função insert)

-No módulo "ver funcionários" logo de cara há o botão de remover o funcionário.(função delete)

-Junto com o botão "remover funcionário" temos o "alterar" que abre um caixa de edição para cada um dos dados.(função edit)
